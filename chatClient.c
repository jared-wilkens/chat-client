#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/*

 * @author Jared Wilkens

 * ACKNOWLEDGMENTS -
 * 
 * I GOT HELP FROM :
 * Ryan K. and I worked toghether on the overall form, flow, and function of the project, we helped each other slove almost all problems. 
 * John R. helped me with memory clearing
 * Tanner L. heleped me with sleep and memset. 
 * Sophia G. helped me with creating a socket
 * Matthew C. general c syntax
 * http://www.qnx.com/developers/docs/6.5.0SP1.update/com.qnx.doc.neutrino_lib_ref/i/inet_pton.html - used to understand how inet_pton works
 * https://www.tutorialspoint.com/c_standard_library/c_function_strcat.htm - used to look at strcat works (not used)
 * http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/client.c - used to undersatnd sockaddrs 
 * https://www.geeksforgeeks.org/taking-string-input-space-c-3-different-methods/ - used to understand scanf (not used)
 * https://stackoverflow.com/questions/8107826/proper-way-to-empty-a-c-string - used to understand memeset 
 * Other basic c syntax look ups 


 * I GAVE HELP TO :
 * Ryan K.
 * Sophia G.
 * Matthew M.

*/ 

int main(int argc, const char* argv[])
{
    
    //***DECLARATIONS***
    //declares strings that will later be used to store both in-coming and out-going messages as well as the user's chosen 
    //strings that will determine when the program quits and when the program receives messages, as well as control flow boolean
    char receive[10]; 
    char quit[10];
    char incoming_message[100] = "filler";
    char out_going[100] = "filler";
    int run = 1;
    
    
    
    
    //***SERVER, SOCKET, AND CONNECTION DATA***
    //creates socket on local machine to be used by server
    int con_sock = socket(AF_INET, SOCK_STREAM, 0);
    if(con_sock < 0){
        
        printf("socket creation failure, terminating program \n");
        goto term;
    }
    
    
    //inet-pton converts IP address to binary form and places this data in data_Store to be used in creates of sockaddr_in
    int error_check_pton;
    struct in_addr data_store;
    error_check_pton = inet_pton(AF_INET, "10.115.20.250", &data_store);
    if(error_check_pton < 0){
        
        printf("inet_pton conversion failure, terminating program \n");
        goto term;
    }
    
    
    //declare and fill sockaddr with server data
    struct sockaddr_in server_sockin;
    server_sockin.sin_family = AF_INET;
    server_sockin.sin_port = htons(49153);
    server_sockin.sin_addr = data_store;
    
    
    
        
    //***USER INPUT, COMMANDS AND USER-NAME***
    // prompts user to enter the string command that the user will use to quit the program later, and captures said string
    printf("Please enter your desired quit command: ");
    fgets(quit,10, stdin);
    
    
    // prompts user to enter the string command that the user will use to read in messages later in the program, and captures said string
    printf("Please enter your desired read messages command: ");
    fgets(receive,10, stdin);
    
    
    //creates username to be later sent as the first line to the server
    memset(out_going, 0, sizeof(out_going));
    printf("Please enter your username: ");
    fgets(out_going,100, stdin);
    out_going[strlen(out_going)+1] = 0;
    
    
    
    
    //***BEGIN CONNECTION AND COMMUNICATION CONTROL FLOW***
    //creates connection to server using created socket and sockaddr_in (casted to sockaddr) data for given IP address
    printf("attempting to connect ...  \n");
    sleep(1);
    int connect_to_server = connect(con_sock, (const struct sockaddr *) &server_sockin, sizeof(server_sockin));
    if(connect_to_server == 0){
        
        printf("connection success \n");

    }
    
    else{
        
       printf("connection failed, terminating program \n");
       goto term;
    }
    
    
    //sends username to server and clears outgoing message memory
    sleep(1);
    send(con_sock, out_going, strlen(out_going), 0);
    memset(out_going, 0, sizeof(out_going));
    
    
    //runs until user tells the progam to stop
    while(run){
        
        
        //gathers user message or command to be used later
        printf("\n \n \n");
        printf("To read new messages use: %sTo quit the progarm use: %sPlease enter command or message: ", receive, quit);
        fgets(out_going,100, stdin);
        printf("Command or message sent to server: %s", out_going);
        
        
        
        
        //checks for quit command and quits
        if(strcmp(quit, out_going)==0){
            
            run = 0;
            sleep(1);
            printf("\n");
        }
        
        
        //checks for receive message command and receives
        else if(strcmp(receive, out_going) == 0){
            
            read(con_sock, &incoming_message, sizeof(incoming_message));
            printf("\n");
            printf("The new message(s) are: \n");
            printf("%s", incoming_message);
            memset(incoming_message, 0, sizeof(incoming_message));
            memset(out_going, 0, sizeof(out_going));
            sleep(1);
        }
        
        
        //sends message if user did not enter quit or receive message command
        else{
            
            send(con_sock, out_going, strlen(out_going),0);
            memset(out_going, 0, sizeof(out_going));
            sleep(1);
        }
    }
    
    
    
    
    //***END OF PROGRAM*** 
    //may be jumped to using term if there is an error earlier in the program, closes socket connection
    term:
    sleep(1);
    printf("Thank you for chatting! \n");
    printf("Closing connection, socket, and progarm ... \n");
    sleep(1);
    close(con_sock);
     
}
